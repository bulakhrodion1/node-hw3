const Router = require('express');
const router = new Router();
const loadsController = require('../controllers/loadsController');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/', authMiddleware, loadsController.getLoadsByUser);
router.post('/', authMiddleware, loadsController.addLoad);
router.get('/active', authMiddleware, loadsController.getActiveLoad);
router.patch('/active/state', authMiddleware, loadsController.changeLoadState);
router.get('/:id', authMiddleware, loadsController.getLoadsById);
router.put('/:id', authMiddleware, loadsController.updateLoadDetails);
router.delete('/:id', authMiddleware, loadsController.deleteLoadById);
router.post('/:id/post', authMiddleware, loadsController.postLoadForUser);
router.get('/:id/shipping_info', authMiddleware, loadsController.getLoadShippingInfo);

module.exports = router;
