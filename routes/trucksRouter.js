const Router = require('express');
const router = new Router();
const trucksController = require('../controllers/trucksController');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/', authMiddleware, trucksController.getTrucks);
router.post('/', authMiddleware, trucksController.addTruck);
router.get('/:id', authMiddleware, trucksController.getTruckById);
router.put('/:id', authMiddleware, trucksController.updateTruckById);
router.delete('/:id', authMiddleware, trucksController.deleteTruckById);
router.post('/:id/assign', authMiddleware, trucksController.assignTruckById);

module.exports = router;
