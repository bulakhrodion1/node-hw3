const Router = require('express');
const router = new Router();
const usersController = require('../controllers/usersController');
const authMiddleware = require('../middleware/authMiddleware');

router.delete('/me', authMiddleware, usersController.deleteUser);
router.get('/me', authMiddleware, usersController.getUser);
router.patch('/me/password', authMiddleware, usersController.changePassUser);

module.exports = router;
