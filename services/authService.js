const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {User} = require('../models/user');
const SECRET_KEY = process.env.SECRET_KEY;

class authService {
    async registration({role, email, password}) {
        const user = new User({
            role,
            email,
            password: await bcrypt.hash(password, 7)
        });
        await user.save();
    }

    async login({email, password}) {
        const user = await User.findOne({email});

        if (!user) {
            throw new Error(`User is not registered`);
        }

        const validPassword = await bcrypt.compareSync(password, user.password);
        if (!validPassword) {
            throw new Error(`Password is incorrect`);
        }

        return jwt.sign({
            _id: user._id,
            email: user.email,
            role: user.role
        }, SECRET_KEY);
    }
}


module.exports = new authService();
