const bcrypt = require('bcrypt');
const { User } = require('../models/user');

class usersService {
    async getUserById(created_by) {
        return await User.findOne({_id: created_by}, '-__v -password');
    }

    async deleteUserById(created_by)  {
        await User.findOneAndRemove({_id: created_by});
    }

    async changeUserPasswordById(created_by, oldPassword, newPassword) {
        await User.findByIdAndUpdate({_id: created_by}, {$set: {password: await bcrypt.hashSync(newPassword, 7)}});
    }
}

module.exports = new usersService();
