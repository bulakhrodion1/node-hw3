const { Truck } = require('../models/truck');

class trucksService {

    async getTrucksByUserId(created_by) {
        return await Truck.find({created_by}, '-__v');
    }

    async addTruckForUser(created_by, truckPayload) {
        const hasTruck = await Truck.findOne({assigned_to: created_by, status: 'OL'});
        if (hasTruck) {
            throw new Error('You are not able to change trucks info while you are on a load');
        }
        const truck = new Truck({...truckPayload, created_by});
        await truck.save();
    }

    async getTruckByIdForUser(truck_id, created_by) {
        return await Truck.findOne({_id: truck_id, created_by}, '-__v');
    }

    async updateTruckByIdForUser(truck_id, created_by, data) {
        const hasTruck = await Truck.findOne({assigned_to: created_by, status: 'OL'});
        if (hasTruck) {
            throw new Error('You are not able to change trucks info while you are on a load');
        }
        await Truck.findByIdAndUpdate({_id: truck_id, created_by}, {$set: data});
    }

    async deleteTruckByIdForUser(truck_id, created_by) {
        const hasTruck = await Truck.findOne({assigned_to: created_by, status: 'OL'});
        if (hasTruck) {
            throw new Error('You are not able to change trucks info while you are on a load');
        }
        await Truck.findOneAndRemove({_id: truck_id, created_by});
    }

    async assignTruckByIdForUser(truck_id, created_by) {
        const hasTruck = await Truck.findOne({assigned_to: created_by, status: 'OL'});
        if (hasTruck) {
            throw new Error('You are not able to change trucks info while you are on a load');
        }
        const truck = await Truck.findOne({_id: truck_id, created_by});
        const { assigned_to } = truck;
        if (assigned_to !== null) {
            throw new Error('Truck is assigned already');
        }
        const trucks = await Truck.find({assigned_to: created_by});
        if (trucks.length !== 0) {
            throw new Error('User has assigment already');
        }
        await Truck.findByIdAndUpdate({_id: truck_id, created_by}, {$set: {assigned_to: created_by}});
    }
}

module.exports = new trucksService();
