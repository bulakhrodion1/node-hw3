const { Load } = require('../models/load');
const { Truck } = require('../models/truck');

class loadsService {
    async getLoadsByUserId(created_by, role, limit=10, offset=0) {
        if (role === 'SHIPPER') {
            return await Load.find({created_by}, '-__v')
                .skip(offset)
                .limit(limit);
        }
        return await Load.find({assigned_to: created_by}, '-__v')
            .skip(offset)
            .limit(limit);
    }

    async addLoadForUser(created_by, loadPayload) {
        const load = new Load({...loadPayload, created_by});
        await load.save();
        await load.update({$push: {logs: {
                    message: `Load was created by shipper with id ${created_by}`,
                    time: new Date(Date.now())
                }}});
    }

    async getUserActiveLoad(created_by) {
        const load = await Load.findOne({assigned_to: created_by});
        load.update({$set: {created_by: load.created_by}});
        return await Load.findOne({assigned_to: created_by}, '-__v');
    }

    async iterateToNextLoadState(created_by) {
        const truck = await Truck.findOne({created_by});
        const load = await Load.findOne({assigned_to: created_by});

        switch (true) {
            case load.state === 'Arrived to delivery':
                throw new Error('Load was arrived to delivery');

            case load.state === 'En route to Pick Up':
                await load.update({$set: {state: 'Arrived to Pick Up'}});
                await load.update({$push: {logs: {
                            message: 'Arrived to Pick Up',
                            time: new Date(Date.now())
                        }}});
                return await Load.findOne({assigned_to: created_by});

            case load.state === 'Arrived to Pick Up':
                await load.update({$set: {state: 'En route to delivery', status: 'SHIPPED'}});
                await load.update({$push: {logs: {
                            message: 'En route to delivery',
                            time: new Date(Date.now())
                        }}});
                return await Load.findOne({assigned_to: created_by});

            case load.state === 'En route to delivery':
                await load.update({$set: {state: 'Arrived to delivery'}});
                await load.update({$push: {logs: {
                            message: 'Arrived to delivery',
                            time: new Date(Date.now())
                        }}});
                await truck.update({$set: {status: 'IS'}});
                return await Load.findOne({assigned_to: created_by});
        }
    }

    async getLoadByIdForUser(load_id, created_by, role) {
        if (role === 'DRIVER') {
            return await Load.findOne({_id: load_id, assigned_to: created_by}, '-__v');
        }
        return await Load.findOne({_id: load_id, created_by}, '-__v');
    }

    async updateLoadByIdForUser(load_id, created_by, data) {
        const load = await Load.findByIdAndUpdate({_id: load_id, created_by}, {$set: data});

        await load.update({$push: {logs: {
                    message: `Load was updated by shipper with id ${created_by}`,
                    time: new Date(Date.now())
                }}});
    }

    async deleteLoadByIdForUser(load_id, created_by) {
        await Load.findOneAndRemove({_id: load_id, created_by});
    }


    async postLoadByIdForUser(load_id, created_by) {
        const load = await Load.findByIdAndUpdate({_id: load_id, created_by}, {$set: {status: 'POSTED'}});
        const trucks = await Truck.find({status: 'IS', assigned_to: { $lt: 'not a number' }}).exec();

        const findDriver = async (load, truck) => {
            if (!truck) {
                await load.update({$set: {status: 'NEW'}});
                await load.update({$push: {logs: {
                            message: `Driver is not found`,
                            time: new Date(Date.now())
                        }}});
                return null;
            }
            await truck.update({$set: {status: 'OL'}});
            await load.update({$set: {
                    assigned_to: truck.assigned_to,
                    status: 'ASSIGNED',
                    state: 'En route to Pick Up'
                }});
            await load.update({$push: {logs: {
                        message: `Load assigned to driver with id ${truck.assigned_to}`,
                        time: new Date(Date.now())
                    }}});
            return truck;
        }

        switch (true) {
            case (load.payload<=1700
                && load.dimensions.width<=300 && load.dimensions.length<=250 && load.dimensions.height<=170):
                return await findDriver(load, trucks[0]);

            case (load.payload<=2500
                && load.dimensions.width<=500 && load.dimensions.length<=250 && load.dimensions.height<=170):
                const truckSm = trucks.find(item => item.type === '!SPRINTER');
                return await findDriver(load, truckSm);

            case (load.payload<=4000
                && load.dimensions.width<=700 && load.dimensions.length<=350 && load.dimensions.height<=200):
                const truckLg = trucks.find(item => item.type === 'LARGE STRAIGHT');
                return await findDriver(load, truckLg);

            case (load.payload>4000
                || load.dimensions.width>700 || load.dimensions.length>350 || load.dimensions.height>200):
                await load.update({$push: {logs: {
                            message: 'Driver is not found',
                            time: new Date(Date.now())
                        }}});
                return null;
        }
    }

    async getLoadShippingInfoByIdForUser(load_id, created_by) {
        const load = await Load.findOne({_id: load_id, created_by}, '-__v');
        const truck = await Truck.findOne({assigned_to: load.assigned_to}, '-__v');
        const loadInfo = {
            load: load,
            truck: truck
        };
        console.log(loadInfo);
        return loadInfo;
    }
}

module.exports = new loadsService();
