const {
    getLoadsByUserId,
    addLoadForUser,
    getUserActiveLoad,
    iterateToNextLoadState,
    getLoadByIdForUser,
    updateLoadByIdForUser,
    deleteLoadByIdForUser,
    postLoadByIdForUser,
    getLoadShippingInfoByIdForUser
} = require('../services/loadsService');

class loadsController {
    async getLoadsByUser(req, res) {
        try {
            const { created_by, role } = req.user;
            let { limit, offset } = req.params;
            const { status } = req.body;

            if (limit>50) {
                limit = 50;
            }

            const loads = await getLoadsByUserId(created_by, role, limit, offset);

            if (status) {
                const filteredGogs = await loads.where({status: {$ne: status}});
                res.status(200).send({filteredGogs});
            }

            res.status(200).send({loads});
        } catch (e) {
            res.status(500).send({message: e.message});
        }
    }

    async addLoad(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'SHIPPER') {
                return res.status(400).send({message:'You do not have the required permissions'});
            }
            await addLoadForUser(created_by, req.body);

            res.status(200).send({message: 'Load created successfully'});
        } catch (e) {
            res.status(500).send({message: e.message});
        }
    }

    async getActiveLoad(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'DRIVER') {
               return res.status(400).send({message:'You do not have the required permissions'});
            }
            const load = await getUserActiveLoad(created_by);

            res.status(200).send({load});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }

    async changeLoadState(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'DRIVER') {
                return res.status(400).send({message:'You do not have the required permissions'});
            }
            const load = await iterateToNextLoadState(created_by);

            res.status(200).send({message: `Load state changed to "${load.state}"`});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }

    async getLoadsById(req, res) {
        try {
            const { created_by, role } = req.user;

            const { id } = req.params;
            const load = await getLoadByIdForUser(id, created_by, role);
            if(!load) {
                return res.status(400).send({message:'No loads found'});
            }

            res.status(200).send({load});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }

    async updateLoadDetails(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'SHIPPER') {
                return res.status(400).send({message:'You do not have the required permissions'});
            }
            const { id } = req.params;
            const data = req.body;
            await updateLoadByIdForUser(id, created_by, data);

            res.status(200).send({message: 'Load details changed successfully'});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }

    async deleteLoadById(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'SHIPPER') {
                return res.status(400).send({message:'You do not have the required permissions'});
            }
            const { id } = req.params;
            await deleteLoadByIdForUser(id, created_by);

            res.status(200).send({message: 'Load deleted successfully'});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }

    async postLoadForUser(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'SHIPPER') {
                return res.status(400).send({message:'You do not have the required permissions'});
            }
            const { id } = req.params;
            const truck = await postLoadByIdForUser(id, created_by);
            if (!truck) {
                return res.status(400).send({message:'Driver is not found'});
            }

            res.status(200).send({
                message: 'Load posted successfully',
                driver_found: true
            });
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }

    async getLoadShippingInfo(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'SHIPPER') {
                return res.status(400).send({message:'You do not have the required permissions'});
            }
            const { id } = req.params;
            const loadInfo = await getLoadShippingInfoByIdForUser(id, created_by);
            if(!loadInfo) {
                return res.status(400).send({message:'No load found'});
            }

            res.status(200).send({loadInfo});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }
}

module.exports = new loadsController();
