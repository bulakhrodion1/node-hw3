const { User } = require("../models/user");

const {
    registration,
    login
} = require('../services/authService');

class authController {
    async register(req, res) {
        try {
            const {role, email, password} = req.body;

            const isUserExist = await User.findOne({email});
            if (isUserExist) {
                return res.status(400).json({message: 'User is already exist'})
            }

            await registration({role, email, password});

            return res.json({message: 'Profile created successfully'});
        } catch (e) {
            res.status(500).json({message: e.message})
        }
    }

    async login(req, res) {
        try {
            const {role, email, password} = req.body;

            const jwt_token = await login({role, email, password});

            return res.status(200).json({message: 'Success', jwt_token});
        } catch (e) {
            res.status(500).json({message: e.message});
        }
    }
}

module.exports = new authController();
