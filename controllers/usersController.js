const { User } = require('../models/user');
const bcrypt = require('bcrypt');

const {
    getUserById,
    deleteUserById,
    changeUserPasswordById
} = require('../services/usersService');

class userController {
    async getUser(req, res) {
        try {
            const { created_by } = req.user;

            const user = await getUserById(created_by);
            if (!user) {
                return res.status(400).send({message: 'Can not get user info'})
            }

            res.status(200).json({
                user: {
                    _id: user._id,
                    role: user.role,
                    email: user.email,
                    createdDate: user.createdDate
                }
            });
        } catch (e) {
            res.status(500).send({message: e.message});
        }
    }

    async deleteUser(req, res) {
        try {
            const { created_by, role } = req.user;

            const user = await getUserById(created_by);
            if (!user) {
                return res.status(400).send({message: 'Can not get user info'})
            }

            if(role === 'DRIVER') {
                return res.status(400).send({message: 'You do not have the required permissions'});
            }

            await deleteUserById(created_by);
            res.status(200).send({message: 'Profile deleted successfully'});
        } catch (e) {
            res.status(500).send({message: e.message});
        }
    }

    async changePassUser(req, res) {
        try {
            const { created_by } = req.user;
            const {oldPassword, newPassword} = req.body;
            const user = await User.findOne({_id: created_by});
            if (!user) {
                return res.status(400).send({message: 'Can not get user info'})
            }

            if (!(await bcrypt.compare(oldPassword, user.password))) {
                return res.status(400).send({message: 'Old password is incorrect'})
            }
            await changeUserPasswordById(created_by, oldPassword, newPassword);

            return res.status(200).send({message: `Password changed successfully`});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }
}

module.exports = new userController();
