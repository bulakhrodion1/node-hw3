const {
    getTrucksByUserId,
    addTruckForUser,
    getTruckByIdForUser,
    updateTruckByIdForUser,
    deleteTruckByIdForUser,
    assignTruckByIdForUser
} = require('../services/trucksService');

class trucksController {
    async getTrucks(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'DRIVER') {
                return res.status(400).json({message: 'You do not have the required permissions'});
            }
            const trucks = await getTrucksByUserId(created_by);
            res.status(200).send({trucks});
        } catch (e) {
            res.status(500).send({message: e.message});
        }
    }

    async addTruck(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'DRIVER') {
                return res.status(400).json({message: 'You do not have the required permissions'});
            }
            await addTruckForUser(created_by, req.body);
            res.status(200).send({message: 'Truck created successfully'});
        } catch (e) {
            res.status(500).send({message: e.message});
        }
    }

    async getTruckById(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'DRIVER') {
                return res.status(400).json({message:'You do not have the required permissions'});
            }
            const { id } = req.params;
            const truck = await getTruckByIdForUser(id, created_by);
            if(!truck) {
                return res.status(400).json({message:'No trucks found'});
            }

            res.status(200).send({truck});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }

    async updateTruckById(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'DRIVER') {
                return res.status(400).json({message:'You do not have the required permissions'});
            }
            const { id } = req.params;
            const data = req.body;
            await updateTruckByIdForUser(id, created_by, data);

            res.status(200).send({message: 'Truck details changed successfully'});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }

    async deleteTruckById(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'DRIVER') {
                return res.status(400).json({message:'You do not have the required permissions'});
            }
            const { id } = req.params;
            await deleteTruckByIdForUser(id, created_by);

            res.status(200).send({message: 'Truck deleted successfully'});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }

    async assignTruckById(req, res) {
        try {
            const { created_by, role } = req.user;

            if(role !== 'DRIVER') {
                return res.status(400).json({message:'You do not have the required permissions'});
            }
            const { id } = req.params;
            await assignTruckByIdForUser(id, created_by);

            res.status(200).send({message: 'Truck assigned successfully'});
        } catch (e) {
            res.status(500).send({message: `Internal server error`});
        }
    }
}

module.exports = new trucksController();
