require("dotenv").config();
const cors = require('cors');

const express = require('express');
const app = express();
const mongoose = require('mongoose');

const PORT = process.env.PORT || 8080;
const DATABASE_CONNECTION = process.env.DATABASE_CONNECTION || null;

const authRouter = require('./routes/authRouter');
const usersRouter = require('./routes/usersRouter');
const loadsRouter = require('./routes/loadsRouter');
const trucksRouter = require('./routes/trucksRouter');

app.use(cors());
app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

const start = async () => {
    try {
        await mongoose.connect(DATABASE_CONNECTION);
        console.log('Database connected');
        app.listen(PORT);
    } catch (e) {
        console.log(`Server error: ${e.message}`);
    }
}

start();
