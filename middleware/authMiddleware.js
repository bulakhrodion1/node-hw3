const jwt = require('jsonwebtoken');
const SECRET_KEY = process.env.SECRET_KEY || null;
module.exports = function(req, res, next) {
    if(req.method === 'OPTIONS') {
        next();
    }
    try {
        const token = req.headers.authorization.split(' ')[1];
        if(!token) {
            return res.status(400).json(({message: 'User is not authorized'}))
        }
        const decodedToken = jwt.verify(token, SECRET_KEY);
        req.user = {
            created_by: decodedToken._id,
            role: decodedToken.role,
            email: decodedToken.email
        }
        next()
    } catch(e) {
        return res.status(400).json(({message: e.message}))
    }
};
